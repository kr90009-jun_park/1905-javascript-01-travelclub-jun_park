const Travel = require('../domain/travelClub');
const store = require('../store/travelClubStore')
const stringUtil = require('../util/StringUtil')
const helper = require('../service/memberHelper')


const MIN_PAGE_SIZE = 10;
const MAX_PAGE_SIZE = 20;


module.exports = class ClubCoordinator {
  constructor() {
    this.storeLogic = new store()
    this.memberHelper = new helper()
  }

  hasClub() {
    //
    return this.storeLogic.count() != 0
  }
  exist(name) {
    
    return this.storeLogic.exist(name)
  }
  register(test) {
    console.log(test.getName())
    if (this.storeLogic.exist(test.getName())) {
      // console.log(test.getName())
      // alert(test.getName())
    }

    this.storeLogic.register(test.getName(), test)
  }
  retrieve(name) {
    //
    console.log("1")
    return this.storeLogic.retrieve(name)

  }
  retrieveAll() {
    //
    return this.storeLogic.retrieveAll();

  }
  update(test, intro) {
    //
    if (!this.storeLogic.exist(test.getName())) {
       return;
    }
    if (!stringUtil.isEmpty(intro)) {
      test.setIntro(intro)
    }

    this.storeLogic.update(test)


  }
  delete(test) {
    //
    console.log("delete")
    this.deleteText(test.getName())

  }
  deleteText(name) {
    //
    if (this.storeLogic.exist(name)) {
      this.storeLogic.delete(name)

    }
  }

  findAll(offset, pageSize) {
    if (!(pageSize === MIN_PAGE_SIZE || pageSize === MAX_PAGE_SIZE)) {
      throw ("Page size should be 10 ")
    }

    return this.storeLogic.retrieveAll(offset, pageSize)
  }

}
