const clubMember = require('../domain/clubMember')
const clubStore = require('../store/clubMemberStore')



module.exports = class MemberHelper{
    constructor(){
    //
    this.clubStoreLogic = new clubStore()
    }

    getMembers(name){
        //
        return this.clubStoreLogic.getMembers(name)
    }

    hashMembers(name){
        //
        return this.clubStoreLogic.hasMembers(name)
    }

    modify(member, newValue){
        //
        this.clubStoreLogic.modify(member, newValue)
    }

    exist(name, email){
        //
        return this.clubStoreLogic.exist(name, email)
    }

    register(name, newMember){
        //
        return this.clubStoreLogic.register(name, newMember)
    }

    find(name, email){
        //
        return this.clubStoreLogic.find(name, email)
    }

    remove(name, memberEmail){
        //
        this.clubStoreLogic.remove(name, memberEmail)
    }
    travel(name , intro){
        this.clubStoreLogic.exist
    }

}