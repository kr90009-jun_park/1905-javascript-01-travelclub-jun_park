
const StringBuilder = require('string-builder')
const RolnInClub = require('./RolnInClub')

module.exports =  class ClubMember {
        constructor(email, name, phoneNumber) {
            this.email = email
            this.name = name;
            this.phoneNumber = phoneNumber
            this.role = RolnInClub.member;
        }
        toString() {
            //
            let string = new StringBuilder();
            string.append("name :").append(this.name)
            string.append(", email ").append(this.email)
            string.append(", nickname ").append(this.nickname)
            string.append(", phone number : ").append(this.phoneNumber)
            string.append(", birthDay : ").appendappend(this.birthday)
            string.append(", role : ").append(this.role)
            return string
        }
        getName() {
            return this.name;
        }

        getEmail() {
            return this.email;
        }

        getNickname() {
            return this.nickname
        }

        getPhoneNumber() {
            return this.phoneNumber;
        }

        getBirthDay(){
            return this.birthday
        }

        getRole() {
            return this.role;
        }

        //getter

        //setter 

        setName(name) {
            this.name = name;
        }

        setEmail(email) {
            if(!this.isValidEmailAddress(email)){
                console.log("Email is not value ")
            }
            this.email = email;
        }
        
        setNickname(nickname){
            this.nickname = nickname
        }

        setPhoneNumber(phoneNumber) {
            this.phoneNumber = phoneNumber;
        }

        setBirthDay(birthDay){
            this.birthday =birthDay
        }

        setRole(role){
            this.role =role
        }


        isValidEmailAddress(email) {
            //
            let ePattern = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$";
            let emailTest = ePattern.match(email);
            console.log(emailTest);

        }


    }
   