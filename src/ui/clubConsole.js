const travel = require("../domain/travelClub")
const serviceMember = require("../service/memberHelper")
const members = require("../domain/clubMember")
const map = require("../store/travelClubStore")
const { ipcRenderer } = require('electron')



// let Logic = new service()

// Logic.register(tests)
// console.log(Logic.retrieveAll())
// console.log(Logic.hasClub())
// console.log(Logic.retrieve("test"))
// console.log(Logic.update(tests, "123123"))
// console.log(Logic.delete(tests))
// console.log(Logic.retrieveAll())


// let member = new serviceMember()

// let memberTest = new members("qktmzlf@naver.com" , "park", "010")



function register() {

    let name = document.getElementById('name').value
    let intro = document.getElementById('intro').value


    ipcRenderer.send("register", name, intro)

    // document.getElementById("demo").inn
    document.getElementById("demo").innerHTML = name
    document.getElementById("demos").innerHTML = intro
    
  

    // var material0 = document.getElementsByTagName('input')[0].value // input에 쓴 값을 변수로 저장(총장 가슴둘레)
    // material0 = material0.split(' '); // 띄어쓰기 단위로 변수를 나눠줌{총장,가슴둘레}
    // var material_an0 = '';
    // for (i = 0; i < material0.length; i++) { // input에 쓴 값 개수만큼 반복문 돌아감(2개)
    //     if (material0 == '') { // 근데 만약 input에 아무것도 안썼다면(value값이 빈칸이라면)
    //         material_an0 = ''; // 아무것도 출력하지 않기
    //     } else { // 만약 제대로 값을 썼다면
    //         material_an0 += '<th>' + material0[i] + '</th>'; // <th></th>태그 안에 값이 순서대로 하나씩 들어감(총장|가슴둘레)
    //     }
    // };
    // document.getElementsByTagName('textarea')[0].innerHTML =
    // '<table><tr><th>size(cm)</th>' +
    // material_an0 + '</tr><tr>' +
    // material_an1 + '</tr><tr>' + material_an2 + '</tr>'

}

function find() {

    let findName = document.getElementById('find').value
    console.log(findName)
   let find= ipcRenderer.sendSync("find-value", findName)
    console.log(find)
    document.getElementById("demo").innerHTML = find.name
    document.getElementById("demos").innerHTML = find.intro
}
// }


function findAll() {
    //
    let findAll = ipcRenderer.sendSync("findAll-value", "test")

    
    document.getElementById("demo").innerHTML = findAll.name
    document.getElementById("demos").innerHTML = findAll.intro
}


function remove() {
    //
    let deleteName = document.getElementById("find").value

    let remove = ipcRenderer.sendSync("remove-value", deleteName)

    alert(remove)

}

function modify() {
    //
    let modifyName = document.getElementById("name").value
    let modifyIntro = document.getElementById("intro").value

    let modify = ipcRenderer.sendSync("modify-value", modifyName, modifyIntro)

    alert("<"+modifyName+">"+"\n"+modifyIntro+"->"+modify.intro+"\n"+"it is change")

    document.getElementById("demo").innerHTML = modify.name
    document.getElementById("demos").innerHTML = modify.intro
}

function displayfindMenuAndGetKey() {
    //
    if (!logic.hasClub()) {
        alert("No more clubs in the storage")
        return "0";

    }
}

// member 

function clubFind() {
    let clubFind = document.getElementById("clubName").value
    ipcRenderer.sendSync("clubFind", clubFind)
}



function clubMember() {
    //
    let email = document.getElementById("email").value
    let name = document.getElementById("name").value
    let phoneNumber = document.getElementById("phoneNumber").value
    let nickName = document.getElementById("nickName").value
    let birthday = document.getElementById("birthday").value
    let roleIn = document.getElementsByName("btn")
    
    let check = ""
    for(let index =0; index<roleIn.length; index++){
        if(roleIn[index].checked){
            check = roleIn[index].value
            console.log(check)
        }
    }

    
    let register = ipcRenderer.sendSync("member-value", email, name, phoneNumber, nickName, birthday, check)
    
    
    document.getElementById("demoa").innerHTML = register.email
    document.getElementById("demob").innerHTML = register.name
    document.getElementById("democ").innerHTML = register.phoneNumber
    document.getElementById("demod").innerHTML = register.nickname
    document.getElementById("demoe").innerHTML = register.role
    document.getElementById("demof").innerHTML = register.birthday

}

function memberfind() {
    //
    let email = document.getElementById("memberFind").value

    let memberFind = ipcRenderer.sendSync("member-find", email)
    
    document.getElementById("demoa").innerHTML = memberFind.email
    document.getElementById("demob").innerHTML = memberFind.name
    document.getElementById("democ").innerHTML = memberFind.phoneNumber
    document.getElementById("demod").innerHTML = memberFind.nickname
    document.getElementById("demoe").innerHTML = memberFind.role
    document.getElementById("demof").innerHTML = memberFind.birthday
    


}

function memberRemove() {
    //
    let email = document.getElementById("memberFind").value

    ipcRenderer.send("member-remove", email)
    alert("The club has been deleted." )
}

function memberModify() {
    //
    let email = document.getElementById("email").value
    let name = document.getElementById("name").value
    let phoneNumber = document.getElementById("phoneNumber").value
    let nickName = document.getElementById("nickName").value
    let birthday = document.getElementById("birthday").value
    let roleIn = document.getElementsByName("btn")

    let check = ""
    for(let index =0; index<roleIn.length; index++){
        if(roleIn[index].checked){
            check = roleIn[index].value
            console.log(check)
        }
    }


    ipcRenderer.sendSync("member-modify", email, name, phoneNumber, nickName, birthday, check)

    
    document.getElementById("demoa").innerHTML = email
    document.getElementById("demob").innerHTML = name
    document.getElementById("democ").innerHTML = phoneNumber
    document.getElementById("demod").innerHTML = nickName
    document.getElementById("demoe").innerHTML = check
    document.getElementById("demof").innerHTML = birthday

}

