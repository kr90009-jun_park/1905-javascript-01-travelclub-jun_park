const { app, BrowserWindow } = require('electron')
const ipcMain = require('electron').ipcMain;
const clubLogic = require('./src/service/clubCoordinator')
const memberLogic = require('./src/service/memberHelper')
const travel = require('./src/domain/travelClub')
const member = require('./src/domain/clubMember')

const clubServiceLogic = new clubLogic()
const memberServiceLogic = new memberLogic()



let clubFind
let memberFind
function createWindow() {
  // 브라우저 창을 생성합니다.
  let win = new BrowserWindow({
    width: 500,
    height: 450,
    webPreferences: {
      nodeIntegration: true
    }
  })




  // and load the index.html of the app.

  win.loadFile('./src/ui/index.html')


}


// clubRegister

ipcMain.on("register", (event, name, intro) => {
  if (name == null || name == "") {
    alert("name should not be null");
}
console.log(name)
if (clubServiceLogic.exist(name)) {
    alert("Club name already exists : " + name)
}
  test = new travel(name, intro)
  clubServiceLogic.register(test)

})

// clubFind

ipcMain.on("find-value", (event , findName) => {
  console.log(findName)
  if(clubServiceLogic.exist(findName)){
  let club = clubServiceLogic.retrieve(findName)
  let obj = JSON.parse(club)
  clubFind = obj.name
  
  event.returnValue = obj
  } else {
    console.log("No such club in the storage")
  }

})

// clubFindAll

ipcMain.on("findAll-value", (event, findAll) => {
  if (clubServiceLogic.hasClub() == 0) {
    console.log("No clubs in the storage")
}
let travelclubs = clubServiceLogic.retrieveAll();

event.returnValue = travelclubs
})

ipcMain.on("remove-value", (event, deleteName) =>{
  let remove = clubServiceLogic.deleteText(deleteName)
  console.log(remove)
    event.returnValue = "The club has been deleted." 
})

ipcMain.on("modify-value", (event, modifyName, modifyIntro) => {
  let update = new travel(modifyName, modifyIntro)
  clubServiceLogic.update(update, modifyIntro)

  event.returnValue = update
})

// member


ipcMain.on("member-value", (event, email, name, phoneNumber, nickName, birthday , role) => {
  let members = new member(email ,name, phoneNumber)
 
  members.setNickname(nickName)
  members.setBirthDay(birthday)
  members.setRole(role)
  let test =  memberServiceLogic.register(clubFind, members)
  
  event.returnValue = test 

  
})

ipcMain.on("member-find", (event, email) => {


  // if(memberServiceLogic.exist(clubFind, email)){

  // }

 let find = memberServiceLogic.find(clubFind, email)
 memberFind = find
  event.returnValue = memberFind
})


ipcMain.on("member-remove", (event , email) => {
  memberServiceLogic.remove(clubFind, email)
})

ipcMain.on("member-modify", (event, email, name, phoneNumber, nickName, birthday, role) => {
  

  let map = new Map()
  map.set("email", email)
  map.set("name", name)
  map.set("phoneNumber", phoneNumber)
  map.set("nickName", nickName)
  map.set("role", role)
  map.set("birthday", birthday)


  // let members = new member(email, name ,phoneNumber)

  // members.setNickname(nickName)
  // members.setBirthDay(birthday)
  // members.setRole(role)
  
  memberServiceLogic.modify(memberFind, map)
  
  
  
  event.returnValue = map
})





// ipcMain.on("find-sync", (event , findName) => {
//   serviceLogic.retrieve(findName)
//   event.returnValue(findName)
// })


app.on('ready', createWindow)